<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Console;

use OpenapiNextGeneration\ApiProjectToolsPhp\Api\OpenapiGeneratorProvider;
use OpenapiNextGeneration\ApiProjectToolsPhp\Route\ActionGenerator;
use OpenapiNextGeneration\ApiProjectToolsPhp\Route\ActionRouteCollector;
use OpenapiNextGeneration\ApiProjectToolsPhp\Route\DispatcherProvider;
use OpenapiNextGeneration\ApiProjectToolsPhp\Test\CodeceptionCestGenerator;
use OpenapiNextGeneration\EntityGeneratorPhp\EntityBuilder;
use OpenapiNextGeneration\JsonSchemaGeneratorPhp\JsonSchemaBuilder;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\PatternMapper;
use Pimple\Container;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateAllCommand extends Command
{
    const OPTION_GENERATE_CESTS = 'generate-cests';


    public function __construct(Container $container, array $options = [])
    {
        parent::__construct('generate-all');
        $this->setDescription('Generate schema.json files, actions, api entities and cests. Flush route cache.');

        $this->setCode(function (InputInterface $input, OutputInterface $output) use ($container, $options) {
            $specification = $container[OpenapiGeneratorProvider::API_SPECIFICATION];

            /* @var PatternMapper $patternMapper */
            $patternMapper = $container[PatternMapper::class];
            $patterns = $patternMapper->buildPatterns($specification);

            $output->writeln('building json-schema files...');
            /* @var JsonSchemaBuilder $jsonSchemaBuilder */
            $jsonSchemaBuilder = $container[JsonSchemaBuilder::class];
            $jsonSchemaBuilder->buildSchemas($specification, 'docs/json-schema');

            $output->writeln('building api entities...');
            /* @var EntityBuilder $entityBuilder */
            $entityBuilder = $container[EntityBuilder::class];
            $entityBuilder->buildEntities($patterns, 'App\\Api', 'src/Api');

            if (is_file(DispatcherProvider::ROUTE_CACHE_FILE)) {
                unlink(DispatcherProvider::ROUTE_CACHE_FILE);
            }
            /* @var ActionGenerator $actionGenerator */
            $actionGenerator = $container[ActionGenerator::class];
            $actionGenerator->generateActions($specification);
            unlink(DispatcherProvider::ROUTE_CACHE_FILE);

            if (($options[self::OPTION_GENERATE_CESTS] ?? true) === true) {
                $cestGenerator = new CodeceptionCestGenerator();
                $cestGenerator->generateActionCests(
                    ActionRouteCollector::collectActionClasses(),
                    $specification
                );
            }
        });
    }
}