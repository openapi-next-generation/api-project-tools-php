<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Test;

use ApiTester;
use OpenapiNextGeneration\GenerationHelperPhp\TargetDirectory;
use PhpParser\PrettyPrinter\Standard;
use PhpParser\PrettyPrinterAbstract;

class CodeceptionCestGenerator
{
    const ACTION_CEST_DIRECTORY = 'tests/Action/';

    protected $specification;
    protected PrettyPrinterAbstract $codePrinter;


    public function __construct()
    {
        $this->codePrinter = new Standard([
            'shortArraySyntax' => true
        ]);
    }

    /**
     * Generate the codeception Cest classes for existing actions
     *
     * $actions should be an array of action reflection classes
     *  create it using ActionRouteCollector::collectActionClasses()
     *
     * @param array $actions
     * @param array $specification
     * @throws \Exception
     */
    public function generateActionCests(array $actions, array $specification)
    {
        $this->specification = $specification;

        /* @var \ReflectionClass $class */
        foreach ($actions as $class) {
            $generator = new ActionCestGenerator($class, $specification);
            preg_match('/Action\/(.+\/)*(.+)\.php/', $class->getFileName(), $matches);
            if (!isset($matches[2])) {
                continue;
            }

            $targetFile = TargetDirectory::getCanonicalTargetDirectory(
                    static::ACTION_CEST_DIRECTORY . $matches[1]
                ) . $matches[2] . 'Cest.php';

            if (!file_exists($targetFile)) {
                file_put_contents(
                    $targetFile,
                    "<?php\n\n" . $this->codePrinter->prettyPrint([$generator->build()])
                );
            }
        }
    }
}