<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Test;

use ApiTester;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Property;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Route;
use PhpParser\BuilderFactory;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Return_;

class ActionCestGenerator
{
    protected \ReflectionClass $action;
    protected array $specification;
    protected BuilderFactory $builder;


    public function __construct(\ReflectionClass $action, array $specification)
    {
        $this->action = $action;
        $this->specification = $specification;
        $this->builder = new BuilderFactory();
    }

    public function build(): Class_
    {
        $class = $this->builder->class($this->action->getShortName() . 'Cest');
        $class->addStmts($this->createBasicMethods());

        return $class->getNode();
    }

    protected function createBasicMethods(): array
    {
        $httpMethod = $this->action->getConstant('HTTP_METHOD');
        $route = $this->action->getConstant('ROUTE');

        $routeHelper = $this->buildRouteHelper($httpMethod, $route);
        $examplePath = $routeHelper->buildExamplePath();
        $exampleRequestBody = $routeHelper->buildExampleRequestBody();
        $schemaFile = 'docs/json-schema/' . $routeHelper->buildResponseJsonSchemaFilePath();

        $statements = [];

        //add basic()
        $statements[] = $basic = $this->builder->method('basic');
        $basic->makePublic();
        $basic->setReturnType('void');
        $basic->addParam($this->builder->param('I')->setType(ApiTester::class));

        //create basic() body
        if ($exampleRequestBody instanceof Property) {
            $basic->addStmt(
                $this->builder->methodCall(
                    $this->builder->var('I'),
                    'haveHttpHeader',
                    [$this->builder->val('Content-Type'), $this->builder->val('application/json')]
                )
            );
            $basic->addStmt(
                $this->builder->methodCall(
                    $this->builder->var('I'),
                    'send' . $httpMethod,
                    [$this->builder->val($examplePath), $this->builder->methodCall($this->builder->var('this'), 'getBasicRequestBody')]
                )
            );
        } else {
            $basic->addStmt(
                $this->builder->methodCall(
                    $this->builder->var('I'),
                    'send' . $httpMethod,
                    [$this->builder->val($examplePath)]
                )
            );
        }
        $basic->addStmt(
            $this->builder->methodCall(
                $this->builder->var('I'),
                'seeResponseCodeIs',
                [$this->builder->val(200)]
            )
        );
        $basic->addStmt(
            $this->builder->methodCall(
                $this->builder->var('I'),
                'seeResponseIsJson'
            )
        );
        if (is_file($schemaFile)) {
            $basic->addStmt(
                $this->builder->methodCall(
                    $this->builder->var('I'),
                    'seeResponseIsValidOnSchemaFile',
                    [$this->builder->val($schemaFile)]
                )
            );
        }

        //add request body provider
        if ($exampleRequestBody instanceof Property) {
            $statements[] = $provider = $this->builder->method('getBasicRequestBody');
            $provider->makePublic();
            $provider->setReturnType('array');
            $provider->addStmt(new Return_($this->builder->val(
                json_decode(json_encode($exampleRequestBody), true)
            )));
        }

        return $statements;
    }

    protected function buildRouteHelper(string $httpMethod, string $route): Route
    {
        return new Route(
            $httpMethod,
            $route,
            $this->specification['paths'][$route][strtolower($httpMethod)] ?? []
        );
    }
}