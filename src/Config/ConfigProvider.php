<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Config;

use Dotenv\Dotenv;
use Noodlehaus\Config;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ConfigProvider implements ServiceProviderInterface
{
    const CONFIG = 'config';


    public function register(Container $container)
    {
        $container[self::CONFIG] = function () {
            Dotenv::create('.')->load();

            return new Config('config/');
        };
    }
}