<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Action;

use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractAction
{
    const HTTP_METHOD = null;
    const ROUTE = null;
    const SCHEMA_FILE = null;
    const REQUEST_ENTITY = null;


    abstract public function __invoke(Request $request, Container $container): Response;
}