<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Application;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ApplicationProvider implements ServiceProviderInterface
{
    public function register(Container $container)
    {
        $container[Application::class] = function (Container $container) {
            return new Application($container);
        };
    }
}