<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Application;

use FastRoute\Dispatcher;
use JsonSchema\Validator;
use OpenapiNextGeneration\ApiProjectToolsPhp\Action\AbstractAction;
use OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiErrors\InvalidSchema;
use OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiErrors\MethodNotAllowed;
use OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiErrors\NotFound;
use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;

class Application implements HttpKernelInterface, TerminableInterface
{
    protected $container;


    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function handle(Request $request, int $type = self::MASTER_REQUEST, bool $catch = true): Response
    {
        /* @var $dispatcher Dispatcher */
        $dispatcher = $this->container[Dispatcher::class];
        $result = $dispatcher->dispatch($request->getMethod(), $request->getBaseUrl() . $request->getPathInfo());
        switch ($result[0]) {
            case Dispatcher::FOUND:
                $request->attributes->add($result[2]);
                $action = new $result[1]();
                $this->onRouteMatch($request, $action);
                return $action($request, $this->container);
            case Dispatcher::NOT_FOUND:
                throw new NotFound();
            case Dispatcher::METHOD_NOT_ALLOWED:
                throw new MethodNotAllowed();
        }
    }

    public function terminate(Request $request, Response $response)
    {

    }

    protected function onRouteMatch(Request $request, AbstractAction $action): void
    {
        if ($action::SCHEMA_FILE !== null) {
            $validator = new Validator();
            $requestBody = json_decode($request->getContent());
            $validator->validate(
                $requestBody,
                json_decode(file_get_contents($action::SCHEMA_FILE))
            );
            if (!$validator->isValid()) {
                throw new InvalidSchema($validator);
            }
        }

        if ($action::REQUEST_ENTITY !== null) {
            $entityClass = $action::REQUEST_ENTITY;
            $request->attributes->set(
                $action::REQUEST_ENTITY,
                new $entityClass(json_decode($request->getContent(), true))
            );
        }
    }
}