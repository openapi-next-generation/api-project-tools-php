<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Api;

use OpenapiNextGeneration\EntityGeneratorPhp\Config\GenerationConfig;
use OpenapiNextGeneration\EntityGeneratorPhp\EntityBuilder;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGenerator;
use OpenapiNextGeneration\JsonSchemaGeneratorPhp\JsonSchemaBuilder;
use OpenapiNextGeneration\JsonSchemaGeneratorPhp\OpenApi\JsonSchemaGenerator;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\PatternMapper;
use OpenapiNextGeneration\OpenapiResolverPhp\AllOfResolver;
use OpenapiNextGeneration\OpenapiResolverPhp\ReferenceResolver;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class OpenapiGeneratorProvider implements ServiceProviderInterface
{
    const API_SPECIFICATION = 'api-specification';
    const API_SPECIFICATION_FILE = 'docs/api.yml';


    public function register(Container $container)
    {
        $container[self::API_SPECIFICATION] = function () {
            $referenceResolver = new ReferenceResolver();
            $specification = $referenceResolver->resolveReference(self::API_SPECIFICATION_FILE)->getValue();

            $specification = $referenceResolver->resolveAllReferences(
                $specification,
                self::API_SPECIFICATION_FILE
            );

            $allOfResolver = new AllOfResolver();
            $specification = $allOfResolver->resolveKeywordAllOf($specification);

            return $specification;
        };

        $container[PatternMapper::class] = function () {
            return new PatternMapper();
        };

        $container[JsonSchemaBuilder::class] = function () {
            return new JsonSchemaBuilder(new JsonSchemaGenerator());
        };

        $container[EntityBuilder::class] = function () {
            return new EntityBuilder(new EntityGenerator(new GenerationConfig()));
        };
    }
}