<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Route;

use FastRoute\RouteCollector;
use OpenapiNextGeneration\ApiProjectToolsPhp\Action\AbstractAction;

class ActionRouteCollector
{
    const ACTION_DIRECTORY = 'src/Action/';
    const ACTION_NAMESPACE = 'App\\Action';

    protected $collector;


    public function __construct(RouteCollector $collector)
    {
        $this->collector = $collector;
    }

    public function addActionRoutes(array $actions)
    {
        /* @var \ReflectionClass $class */
        foreach ($actions as $class) {
            $this->collector->addRoute(
                $class->getConstant('HTTP_METHOD'),
                $class->getConstant('ROUTE'),
                '\\' . $class->getName()
            );
        }
    }

    public static function collectActionClasses(): array
    {
        $actions = [];

        $it = new \RecursiveDirectoryIterator(self::ACTION_DIRECTORY, \FilesystemIterator::SKIP_DOTS);
        /* @var \SplFileInfo $file */
        foreach (new \RecursiveIteratorIterator($it) as $file) {
            preg_match('/.*\/Action\/(.*)\.php/', $file->getPathname(), $matches);
            if (!isset($matches[1])) {
                continue;
            }
            try {
                $class = new \ReflectionClass(
                    '\\' . self::ACTION_NAMESPACE . '\\' . str_replace('/', '\\', $matches[1])
                );
                if (!$class->isSubclassOf(AbstractAction::class)) {
                    continue;
                }
                $actions[] = $class;
            } catch (\ReflectionException $exception) {
                continue;
            }
        }

        return $actions;
    }
}