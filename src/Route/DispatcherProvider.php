<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Route;

use function FastRoute\cachedDispatcher;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class DispatcherProvider implements ServiceProviderInterface
{
    const ROUTE_CACHE_FILE = 'var/cache/routes.php';


    public function register(Container $container)
    {
        $container[Dispatcher::class] = function (Container $container) {
            return cachedDispatcher(
                function (RouteCollector $collector) {
                    $actionCollector = new ActionRouteCollector($collector);
                    $actionCollector->addActionRoutes(ActionRouteCollector::collectActionClasses());
                },
                [
                    'cacheFile' => self::ROUTE_CACHE_FILE
                ]
            );
        };
    }
}