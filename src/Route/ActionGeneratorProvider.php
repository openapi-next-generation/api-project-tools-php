<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Route;

use FastRoute\Dispatcher;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ActionGeneratorProvider implements ServiceProviderInterface
{
    public function register(Container $container)
    {
        $container[ActionGenerator::class] = function (Container $container) {
            return new ActionGenerator($container[Dispatcher::class]);
        };
    }
}