<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Route;

use FastRoute\Dispatcher;
use OpenapiNextGeneration\ApiProjectToolsPhp\Action\AbstractAction;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\UseCollector;
use OpenapiNextGeneration\GenerationHelperPhp\TargetDirectory;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Route;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\RoutesSpecification;
use PhpParser\BuilderFactory;
use PhpParser\Node\Const_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\Return_;
use PhpParser\PrettyPrinter\Standard;
use PhpParser\PrettyPrinterAbstract;
use Pimple\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ActionGenerator
{
    protected Dispatcher $dispatcher;
    protected BuilderFactory $builder;
    protected PrettyPrinterAbstract $codePrinter;
    protected UseCollector $useCollector;


    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->builder = new BuilderFactory();
        $this->codePrinter = new Standard([
            'shortArraySyntax' => true
        ]);
    }

    public function generateActions(array $specification): void
    {
        $routesSpecification = new RoutesSpecification($specification);
        /* @var Route $route */
        foreach ($routesSpecification->getUniqueRoutes() as $route) {
            $result = $this->dispatcher->dispatch($route->getHttpMethod(), $route->getPath());
            if ($result[0] !== Dispatcher::FOUND) {
                $this->createAction($route);
            }
        }
    }

    protected function createAction(Route $route): void
    {
        $actionName = $this->createActionName($route);

        $namespaceName = ActionRouteCollector::ACTION_NAMESPACE;
        $actionDirectory = ActionRouteCollector::ACTION_DIRECTORY;
        $mainTag = $route->getTags()[0] ?? null;
        if (!empty($mainTag)) {
            $namespaceName .= '\\' . ucfirst($mainTag);
            $actionDirectory .= ucfirst($mainTag) . '/';
        }
        $this->useCollector = new UseCollector($namespaceName);
        $namespace = $this->builder->namespace($namespaceName);

        $action = $this->builder->class($actionName);
        $action->extend($this->useCollector->useClass(AbstractAction::class));

        $action->addStmts($this->createActionConstants($route));

        $invoke = $this->builder->method('__invoke');
        $invoke->makePublic();
        $invoke->addParam($this->builder->param('request')->setType($this->useCollector->useClass(Request::class)));
        $invoke->addParam($this->builder->param('container')->setType($this->useCollector->useClass(Container::class)));
        $invoke->setReturnType($this->useCollector->useClass(Response::class));
        $invoke->addStmt(new Return_($this->builder->new($this->useCollector->useClass(JsonResponse::class))));
        $action->addStmt($invoke);

        $namespace->addStmts($this->useCollector->buildUseStatements());
        $namespace->addStmt($action);

        file_put_contents(
            TargetDirectory::getCanonicalTargetDirectory($actionDirectory)
            . $actionName . '.php',
            "<?php\n\n" . $this->codePrinter->prettyPrint([$namespace->getNode()])
        );
    }

    protected function createActionName(Route $route): string
    {
        $rawName = str_replace(['-', '_', '/'], '', ucwords($route->getPathName(), '-_/'));
        return ucfirst(strtolower($route->getHttpMethod())) . ucfirst($rawName);
    }

    protected function createActionConstants(Route $route): array
    {
        $statements = [
            new ClassConst([new Const_('HTTP_METHOD', $this->builder->val($route->getHttpMethod()))]),
            new ClassConst([new Const_('ROUTE', $this->builder->val($route->getPath()))])
        ];

        $requestBody = $route->getMethodSpecification()['requestBody'] ?? null;
        if ($requestBody !== null) {
            $statements[] = new ClassConst([new Const_(
                'SCHEMA_FILE',
                $this->builder->val('docs/json-schema/' . $route->buildRequestJsonSchemaFilePath())
            )]);

            $entity = $requestBody['content']['application/json']['schema']['$ref'] ?? null;
            if ($entity !== null) {
                $class = 'App\\Api\\Entities\\' . basename($entity);
                if (class_exists('\\' . $class)) {
                    $statements[] = new ClassConst([new Const_(
                        'REQUEST_ENTITY',
                        $this->builder->classConstFetch($this->useCollector->useClass($class), 'class')
                    )]);
                }
            }
        }

        return $statements;
    }
}