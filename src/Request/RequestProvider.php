<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Request;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class RequestProvider implements ServiceProviderInterface
{
    public function register(Container $container)
    {
        $container[Request::class] = function () {
            return Request::createFromGlobals();
        };
    }
}