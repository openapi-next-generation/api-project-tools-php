<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Error;

use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class LoggerProvider implements ServiceProviderInterface
{
    public function register(Container $container)
    {
        $container[Logger::class] = function () {
            $formatter = new JsonFormatter();

            $logHandler = new StreamHandler('php://stdout');
            $logHandler->setFormatter($formatter);

            return new Logger('log', [$logHandler]);
        };
    }
}