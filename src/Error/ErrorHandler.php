<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Error;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\Response;

class ErrorHandler
{
    const FATAL_ERRORS = [
        E_ERROR => true,
        E_PARSE => true,
        E_CORE_ERROR => true,
        E_COMPILE_ERROR => true,
        E_USER_ERROR => true
    ];

    protected static $instance;
    /**
     * @var LoggerInterface
     */
    protected $logger;


    public static function register(): void
    {
        $instance = static::$instance = new static();
        set_exception_handler([$instance, 'handleThrowable']);
        set_error_handler([$instance, 'handleError']);
        register_shutdown_function(function () use ($instance) {
            $error = error_get_last();
            if ($error !== null) {
                $instance->handleError($error['type'], $error['message'], $error['file'], $error['line']);
            }
        });
    }

    public static function instance(): self
    {
        return static::$instance;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function log(ApiError $apiError)
    {
        $this->logThrowable($apiError, $apiError::LOG_LEVEL);
    }

    public function sendErrorResponse(\Throwable $throwable = null): void
    {
        if (!$throwable instanceof ApiError) {
            $throwable = new ApiError();
            $throwable->addMessage(
                Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        $throwable->buildResponse()->send();
    }

    public function handleThrowable(\Throwable $throwable): void
    {
        $this->sendErrorResponse($throwable);
        if ($throwable instanceof ApiError) {
            $logLevel = $throwable::LOG_LEVEL;
        } else {
            $logLevel = LogLevel::ALERT;
        }
        $this->logThrowable($throwable, $logLevel);
    }

    public function handleError($code, $message, $file = '', $line = 0, $context = []): void
    {
        if (isset(static::FATAL_ERRORS[$code])) {
            $this->sendErrorResponse();
        }
        $this->logError($message, $file, $line);
    }

    protected function __construct() {

    }

    protected function logThrowable(\Throwable $throwable, string $logLevel): void
    {
        $this->logError(
            $throwable->getMessage(),
            $throwable->getFile(),
            $throwable->getLine(),
            $logLevel
        );
    }

    protected function logError(
        $message,
        $file = '',
        $line = 0,
        string $logLevel = LogLevel::ALERT
    ): void
    {
        $this->logger->log(
            $logLevel,
            $message,
            [
                'file' => $file,
                'line' => $line
            ]
        );
    }
}