<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Error;

use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiError extends \Exception
{
    const HTTP_STATUS_CODE = Response::HTTP_INTERNAL_SERVER_ERROR;
    const LOG_LEVEL = LogLevel::INFO;

    protected $messages = [];


    public function __construct()
    {

    }

    public function addMessage(
        string $detail,
        int $httpStatusCode = Response::HTTP_BAD_REQUEST,
        string $id = null
    ): void
    {
        $this->messages[] = [
            'id' => $id,
            'detail' => $detail,
            'status' => $httpStatusCode
        ];
    }

    public function buildResponse(): Response
    {
        $this->message = json_encode([
            'errors' => $this->messages
        ]);
        $response = new JsonResponse(
            $this->message,
            static::HTTP_STATUS_CODE,
            [],
            true
        );

        return $response;
    }
}