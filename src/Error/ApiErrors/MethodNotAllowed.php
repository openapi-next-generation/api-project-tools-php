<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiErrors;

use OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiError;
use Symfony\Component\HttpFoundation\Response;

class MethodNotAllowed extends ApiError
{
    const HTTP_STATUS_CODE = Response::HTTP_METHOD_NOT_ALLOWED;


    public function __construct()
    {
        $this->addMessage(Response::$statusTexts[static::HTTP_STATUS_CODE], static::HTTP_STATUS_CODE);
    }
}