<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiErrors;

use JsonSchema\Validator;
use OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiError;
use Symfony\Component\HttpFoundation\Response;

class InvalidSchema extends ApiError
{
    const HTTP_STATUS_CODE = Response::HTTP_BAD_REQUEST;


    public function __construct(Validator $validator)
    {
        foreach ($validator->getErrors() as $error) {
            $this->messages[] = [
                'id' => null,
                'detail' => $error['message'],
                'status' => static::HTTP_STATUS_CODE,
                'source' => [
                    'pointer' => $error['pointer']
                ]
            ];
        }
    }
}