<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiErrors;

use OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiError;
use Symfony\Component\HttpFoundation\Response;

class Forbidden extends ApiError
{
    const HTTP_STATUS_CODE = Response::HTTP_FORBIDDEN;


    public function __construct()
    {
        $this->addMessage(Response::$statusTexts[static::HTTP_STATUS_CODE], static::HTTP_STATUS_CODE);
    }
}