<?php

namespace OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiErrors;

use OpenapiNextGeneration\ApiProjectToolsPhp\Error\ApiError;
use Symfony\Component\HttpFoundation\Response;

class NotFound extends ApiError
{
    const HTTP_STATUS_CODE = Response::HTTP_NOT_FOUND;


    public function __construct()
    {
        $this->addMessage(Response::$statusTexts[static::HTTP_STATUS_CODE], static::HTTP_STATUS_CODE);
    }
}